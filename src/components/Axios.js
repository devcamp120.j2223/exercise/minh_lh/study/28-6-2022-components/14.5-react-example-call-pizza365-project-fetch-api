import { Component } from "react";
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";

class Axios extends Component {

    axiosLibrary = async (url, body) => {
        let response = await axios(url, body);
        return response.data;
    }
    getAll = () => {
        this.axiosLibrary("http://42.115.221.44:8080/devcamp-pizza365/orders")
            .then((data) => {
                console.log(data);
            })
    }
    createOrder = () => {
        axios.post('http://42.115.221.44:8080/devcamp-pizza365/orders', {
            kichCo: "M",
            duongKinh: "25",
            suon: "4",
            salad: "300",
            loaiPizza: "HAWAII",
            idVourcher: "16512",
            idLoaiNuocUong: "PEPSI",
            soLuongNuoc: "3",
            hoTen: "Phạm Thanh Bình",
            thanhTien: "200000",
            email: "binhpt001@devcamp.edu.vn",
            soDienThoai: "0865241654",
            diaChi: "Hà Nội",
            loiNhan: "Pizza đế dày"
        }, {
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            }
        })
            .then(response => {
                console.log(response)
            })
            .catch(error => {
                console.log(error.response)
            });
    }
    getOrderById = () => {
        this.axiosLibrary("http://42.115.221.44:8080/devcamp-pizza365/orders/jizSrozrtO")
            .then((data) => {
                console.log(data);
            })
    }
    updateOrder = () => {
        let body = {
            method: "PUT",
            body: JSON.stringify({
                trangThai: "confirmed",
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        };
        this.axiosLibrary("http://42.115.221.44:8080/devcamp-pizza365/orders/VpsaVIb7hG", axios.body)
            .then((data) => {
                console.log(data);
            })
    }
    checkVoucherById = () => {
        this.axiosLibrary("http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/12332")
            .then((data) => {
                console.log(data);
            })
    }
    getDrinkList = () => {
        this.axiosLibrary("http://42.115.221.44:8080/devcamp-pizza365/drinks")
            .then((data) => {
                console.log(data);
            })
    }
    render() {
        return (
            <div className="container mt-5">
                <h1>Axios Library</h1>
                <div className="row ">
                    <div className="col-2">
                        <button className="btn btn-primary p-2" onClick={this.getAll}>Get All Orders!</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-info p-2" onClick={this.createOrder}>Create Order!</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-success p-2" onClick={this.getOrderById}>Get Order By Id!</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-secondary p-2" onClick={this.updateOrder}>Update Order!</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-danger p-2" onClick={this.checkVoucherById}>Check Voucher By Id!</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-success p-2" onClick={this.getDrinkList}>Get drink list!</button>
                    </div>
                </div>

            </div>

        )
    }
}
export default Axios;