import { Component } from "react";
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";

class FetchApi extends Component {

    fetchApi = async (url, body) => {
        let response = await fetch(url, body);
        let data = await response.json();
        return data;
    }
    getAll = () => {
        this.fetchApi("http://42.115.221.44:8080/devcamp-pizza365/orders")
            .then((data) => {
                console.log(data);
            })
    }
    createOrder = () => {
        let body = {
            method: "POST",
            body: JSON.stringify({
                kichCo: "M",
                duongKinh: "25",
                suon: "4",
                salad: "300",
                loaiPizza: "HAWAII",
                idVourcher: "16512",
                idLoaiNuocUong: "PEPSI",
                soLuongNuoc: "3",
                hoTen: "Phạm Thanh Bình",
                thanhTien: "200000",
                email: "binhpt001@devcamp.edu.vn",
                soDienThoai: "0865241654",
                diaChi: "Hà Nội",
                loiNhan: "Pizza đế dày"
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        };
        this.fetchApi("http://42.115.221.44:8080/devcamp-pizza365/orders", body)
            .then((data) => {
                console.log(data);
            })
    }
    getOrderById = () => {
        this.fetchApi("http://42.115.221.44:8080/devcamp-pizza365/orders/MCrIi3j0Sx")
            .then((data) => {
                console.log(data);
            })
    }
    updateOrder = () => {
        let body = {
            method: "PUT",
            body: JSON.stringify({
                trangThai: "confirmed",
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        };
        this.fetchApi("http://42.115.221.44:8080/devcamp-pizza365/orders/VpsaVIb7hG", fetch.body)
            .then((data) => {
                console.log(data);
            })
    }
    checkVoucherById = () => {
        this.fetchApi("http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/12332")
            .then((data) => {
                console.log(data);
            })
    }
    getDrinkList = () => {
        this.fetchApi("http://42.115.221.44:8080/devcamp-pizza365/drinks")
            .then((data) => {
                console.log(data);
            })
    }
    render() {
        return (
            <div className="container mt-5">
                <h1>Fetch library</h1>
                <div className="row ">
                    <div className="col-2">
                        <button className="btn btn-primary p-2" onClick={this.getAll}>Get All Orders!</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-info p-2" onClick={this.createOrder}>Create Order!</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-success p-2" onClick={this.getOrderById}>Get Order By Id!</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-secondary p-2" onClick={this.updateOrder}>Update Order!</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-danger p-2" onClick={this.checkVoucherById}>Check Voucher By Id!</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-success p-2" onClick={this.getDrinkList}>Get drink list!</button>
                    </div>
                </div>

            </div>

        )
    }
}
export default FetchApi;