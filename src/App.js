import FetchApi from "./components/FetchApi"
import Axios from "./components/Axios"
function App() {
  return (
    <div>
      <FetchApi />
      <Axios />
    </div>
  );
}

export default App;
